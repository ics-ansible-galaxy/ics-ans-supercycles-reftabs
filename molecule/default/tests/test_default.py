import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('supercycles_reftabs')


def test_reftabs_installed(host):
    assert host.file("/opt/reftabs/supercycles").exists
